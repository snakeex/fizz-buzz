import FizzBuzz from "./components/FizzBuzz";

function App() {
  return <FizzBuzz from={1} to={100} />;
}

export default App;
