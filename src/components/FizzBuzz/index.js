import React from "react";
import PropTypes from "prop-types";

import "./style.css";

export const createNumberRange = (start, end) =>
  Array.from({ length: end - start + 1 }, (_, k) => k + start);

export const mapNumberByDividers = (number, mapping) => {
  let result = "";
  Object.keys(mapping).forEach((divider) => {
    if (number % divider === 0) {
      result = result + mapping[divider];
    }
  });
  return result !== "" ? result : number;
};

const FizzBuzz = (props) => {
  const { from, to } = { ...props };
  const dividerMapping = { 3: "Fizz", 5: "Buzz" };

  return (
    <div id="result-container">
      {createNumberRange(from, to).map((number, index) => (
        <span key={index} className="result-item">
          {mapNumberByDividers(number, dividerMapping)}
        </span>
      ))}
    </div>
  );
};

FizzBuzz.propTypes = {
  from: PropTypes.number,
  to: PropTypes.number,
};

FizzBuzz.defaultProps = {
  from: 1,
  to: 100,
};

export default FizzBuzz;
