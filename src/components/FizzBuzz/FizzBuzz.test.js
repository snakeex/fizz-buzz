import { render } from "@testing-library/react";
import FizzBuzz, { createNumberRange, mapNumberByDividers } from "./index";

test("Renders correctly", () => {
  render(<FizzBuzz />);
});

test("Creates number array with 100 elements", () => {
  const from = 1;
  const to = 100;
  const range = createNumberRange(from, to);
  expect(range.length).toEqual(100);
});

describe("Test numbers", () => {
  const dividerMapping = { 3: "Fizz", 5: "Buzz" };

  test("3 should 'Fizz' number", () => {
    const result = mapNumberByDividers(3, dividerMapping);
    expect(result).toEqual("Fizz");
  });

  test("5 should 'Buzz' number", () => {
    const result = mapNumberByDividers(5, dividerMapping);
    expect(result).toEqual("Buzz");
  });

  test("15 should 'FizzBuzz' number", () => {
    const result = mapNumberByDividers(15, dividerMapping);
    expect(result).toEqual("FizzBuzz");
  });

  test("16 should not mapped", () => {
    const result = mapNumberByDividers(16, dividerMapping);
    expect(result).toEqual(16);
  });
});
